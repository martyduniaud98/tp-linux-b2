# Module 2 : Réplication de base de données

- Install mariadb sur le master et le slave
```bash
[marty@master ~]$ sudo dnf install mariadb-server -y
[marty@master ~]$ sudo systemctl start mariadb
[marty@master ~]$ sudo systemctl enable mariadb
```

- Préparation de la Vm master
```bash
[marty@master ~]$ sudo nano /etc/my.cnf
[marty@master ~]$ cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

[mysqld]
bind-address            = 0.0.0.0
server-id              = 1
max_binlog_size        = 100M
relay_log = /var/log/mariadb/mysql-relay-bin
relay_log_index = /var/log/mariadb/mysql-relay-bin.index
log_bin        = /var/log/mariadb/mysql-bin.log

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d


[marty@master ~]$ sudo systemctl restart mariadb
```

- Créer un user de réplication sur la Vm master
```sql
MariaDB [(none)]> CREATE USER 'replication'@'%' identified by 'securepassword';
Query OK, 0 rows affected (0.013 sec)

MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
Query OK, 0 rows affected (0.012 sec)

MariaDB [(none)]>  FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000001 |      328 |              |                  |
+------------------+----------+--------------+------------------+
1 row in set (0.000 sec)
```

- Préparation de réplication de la Vm slave
``` bash
[marty@master ~]$ sudo nano /etc/my.cnf
[marty@master ~]$ cat /etc/my.cnf
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

[mysqld]
bind-address            = 0.0.0.0
server-id              = 2
log_bin                = /var/log/mariadb/mysql-bin.log
max_binlog_size        = 100M
relay_log = /var/log/mariadb/mysql-relay-bin
relay_log_index = /var/log/mariadb/mysql-relay-bin.index
#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

[marty@master ~]$ sudo systemctl restart mariadb

[marty@slave ~]$ mysql -u root -p
MariaDB [(none)]> stop slave;
Query OK, 0 rows affected, 1 warning (0.000 sec)
MariaDB [(none)]> CHANGE MASTER TO MASTER_HOST = '10.102.1.12', MASTER_USER = 'replication', MASTER_PASSWORD = 'securepassword', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 328;
MariaDB [(none)]> start slave;
Query OK, 0 rows affected (0.002 sec)
```

- Vérification de la réplication mariadb
```bash
# db master
mysql -u root -p
MariaDB [(none)]> CREATE DATABASE schooldb;
MariaDB [(none)]> USE schooldb;
MariaDB [schooldb]>

[marty@master ~]$ sudo mysqldump -u root nextcloud > nextcloud.sql
[marty@master ~]$ scp nextcloud.sql marty@10.102.1.14:/home/marty/

# db slave
[marty@slave ~]$ sudo nano nextcloud.sql
[marty@slave ~]$ cat nextcloud.sql | head -n 2
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
use nextcloud;

[marty@slave ~]$ sudo mysql -u root < nextcloud.sql 
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
| schooldb           |
+--------------------+
5 rows in set (0.001 sec)
```


✨ **Bonus** : Faire en sorte que l'utilisateur créé en base de données ne soit utilisable que depuis l'autre serveur de base de données

- inspirez-vous de la création d'utilisateur avec `CREATE USER` effectuée dans le TP2

✨ **Bonus** : Mettre en place un setup *master-master* où les deux serveurs sont répliqués en temps réel, mais les deux sont capables de traiter les requêtes.
