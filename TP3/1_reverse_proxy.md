# Module 1 : Reverse Proxy

## Sommaire

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
  - [Sommaire](#sommaire)
- [I. Intro](#i-intro)
- [II. Setup](#ii-setup)
- [III. HTTPS](#iii-https)

# I. Intro

# II. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`
```bash
[marty@proxy ~]$ sudo dnf install nginx -y
```

- démarrer le service `nginx`
```bash 
[marty@proxy ~]$ sudo systemctl start nginx
```

- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute
```bash
[marty@proxy ~]$ sudo ss -laptn | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*     users:(("nginx",pid=1068,fd=6),("nginx",pid=1067,fd=6))
LISTEN 0      511             [::]:80           [::]:*     users:(("nginx",pid=1068,fd=7),("nginx",pid=1067,fd=7))
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX
```bash
[marty@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX
```bash
[marty@proxy ~]$ ps -ef | grep "nginx: worker"
nginx       1068    1067  0 15:58 ?        00:00:00 nginx: worker process
```

- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine
```bash
[marty@proxy ~]$ curl localhost:80
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
[...]
```

➜ **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence
    ```bash
    [marty@proxy etc]$ sudo nano /etc/nginx/conf.d/nginx.conf
    [marty@proxy etc]$ sudo cat /etc/nginx/conf.d/nginx.conf
    server {
        # On indique le nom que client va saisir pour accéder au service
        # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
        server_name web.tp2.linux;

        # Port d'écoute de NGINX
        listen 80;

        location / {
            # On définit des headers HTTP pour que le proxying se passe bien
            proxy_set_header  Host $host;
            proxy_set_header  X-Real-IP $remote_addr;
            proxy_set_header  X-Forwarded-Proto https;
            proxy_set_header  X-Forwarded-Host $remote_addr;
            proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

            # On définit la cible du proxying 
            proxy_pass http://10.102.1.11:80;
        }

        # Deux sections location recommandés par la doc NextCloud
        location /.well-known/carddav {
        return 301 $scheme://$host/remote.php/dav;
        }

        location /.well-known/caldav {
        return 301 $scheme://$host/remote.php/dav;
        }
    }
    ```

  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`
    ```bash
    [marty@web config]$ sudo cat config.php 
    <?php
    $CONFIG = array (
    'instanceid' => 'oce82init832',
    'passwordsalt' => 'wSJVwXeDickWbZEJMWnNvIQYZwI2+C',
    'secret' => 'y17cwsEs2B7Z2j0w/7fmcJzhKApndeS/gm2QArZfuWFmD7rP',
    'trusted_domains' => 
    array (
        0 => 'web.tp2.linux',
        1 => '10.102.1.13',
        2 => 'proxy.tp3.linux'
    ),
    'trusted_proxies' => '10.102.1.13',
    'datadirectory' => '/var/www/tp2_nextcloud/data',
    'dbtype' => 'mysql',
    'version' => '25.0.0.15',
    'overwrite.cli.url' => 'http://web.tp2.linux',
    'dbname' => 'nextcloud',
    'dbhost' => '10.102.1.12:3306',
    'dbport' => '',
    'dbtableprefix' => 'oc_',
    'mysql.utf8mb4' => true,
    'dbuser' => 'nextcloud',
    'dbpassword' => 'martydb',
    'installed' => true,
    );
    ```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp2.linux`
- c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`
```bash
marty@martywork-ROGFlowX13:~$ cat /etc/hosts | grep tp2
10.102.1.13     web.tp2.linux 
```
- autrement dit, pour votre PC :
  - `web.tp2.linux` pointe vers l'IP du reverse proxy
  - `proxy.tp3.linux` ne pointe vers rien
  - taper `http://web.tp2.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)
  ```bash
  # le code 302 indique bien que web.tp2.linux fonctionne mais est redirigé
  marty@martywork-ROGFlowX13:~$ curl http://web.tp2.linux -I | head -n1
    HTTP/1.1 302 Found
  ```

> Oui vous ne rêvez pas : le nom d'une machine donnée pointe vers l'IP d'une autre ! Ici, on fait juste en sorte qu'un certain nom permette d'accéder au service, sans se soucier de qui porte réellement ce nom.

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal.

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`
  - une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
  - l'autre est la clé publique : elle sera stockée dans un fichier appelé *certificat*
    - le *certificat* est donné à chaque client qui se connecte au site
    ```bash
    [marty@proxy ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
    [marty@proxy ~]$ sudo mv server.crt web.tp2.linux.crt
    [marty@proxy ~]$ sudo mv server.key web.tp2.linux.key
    [marty@proxy srv]$ sudo mv web.tp2.linux.crt /etc/pki/tls/certs/
    [marty@proxy srv]$ sudo mv web.tp2.linux.key /etc/pki/tls/private/
    ```
    
- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP
  ```bash
  [marty@proxy ~]$ sudo cat /etc/nginx/conf.d/nginx.conf | grep ssl
        listen 443 ssl;
        ssl_certificate     /etc/pki/tls/certs/web.tp2.linux.crt;
        ssl_certificate_key /etc/pki/tls/private/web.tp2.linux.key;

  [marty@proxy ~]$ chown nginx:nginx /etc/pki/tls/certs/web.tp2.linux.crt
  [marty@proxy ~]$ chown nginx:nginx /etc/pki/tls/private/web.tp2.linux.key
  ```

- redirection http vers https 
```bash
[marty@proxy ~]$ cat /etc/nginx/conf.d/nginx.conf 
[...]
server {

    listen 80 default_server;


    server_name web.tp2.linux;


    return 301 https://web.tp2.linux/index.php/apps/dashboard/;

}
```

Je vous laisse Google vous-mêmes "nginx reverse proxy nextcloud" ou ce genre de chose :)

