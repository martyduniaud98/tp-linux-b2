# Module 5 : Monitoring

Dans ce sujet on va installer un outil plutôt clé en main pour mettre en place un monitoring simple de nos machines.


Installation et conf:
```bash
[marty@web ~]$ sudo dnf install epel-release -y
[marty@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh

[marty@web ~]$ sudo systemctl start netdata
[marty@web ~]$ sudo systemctl enable netdata

[marty@web ~]$ sudo ss -ltpn | grep netdata                                                                                           
LISTEN 0      4096         0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2780,fd=6))  

[marty@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
[marty@web ~]$ sudo firewall-cmd --reload
success
```

- Stress test Memory
```bash
[marty@web ~]$ cd /etc/netdata/health.d
[marty@web health.d]$ sudo nano test_ram.conf
[marty@web ~]$ cat /etc/netdata/health.d/test_ram.conf 
alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 80
  crit: $this > 90
  info: The percentage of RAM being used by the system.
```

- Message d'alarme discord 
```
web.tp2.linux needs attention, system.swapio (swap), 30min ram swapped out = 753.3% of RAM
30min ram swapped out = 753.3% of RAM
percentage of the system RAM swapped in the last 30 minutes
system.swapio
swap

web.tp2.linux•Aujourd’hui à 15:20
```

