# Module 7 : Fail2Ban

Faites en sorte que :

- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
- vérifiez que ça fonctionne en vous faisant ban
- afficher la ligne dans le firewall qui met en place le ban
- lever le ban avec une commande liée à fail2ban


- Install du référenciel EPEL
```bash
[marty@web ~]$ sudo dnf install epel-release
```

- Install fail2ban et ses dépendences
```bash
[marty@web ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
```

- Allumage fail2ban + allumage au démarrage
```bash
[marty@web ~]$ sudo systemctl start fail2ban
[marty@web ~]$ sudo systemctl enable fail2ban
```

- Conf ban mot de passe incorrect
```bash 
[marty@web ~]$ cat /etc/fail2ban/jail.d/sshd.local
[sshd] 
enabled = true 

# Remplacer la configuration globale par défaut 
# pour une prison spécifique sshd 
bantime = 1h 
maxretry = 3
findtime = 1m
```

- Test ban 
```bash
[marty@web ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed: 0
|  |- Total failed:     3
|  `- Journal matches:  _SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned: 1
   |- Total banned:     1
   `- Banned IP list:   10.102.1.12
```

- Vérifier la règle dans le firewall 
```bash
[marty@web ~]$ sudo firewall-cmd --list-all
public (active)
  [...]
  rich rules: 
        rule family="ipv4" source address="10.102.1.12" port port="ssh" protocol="tcp" reject type="icmp-port-unreac
```

- Unban une IP
```bash
[marty@web ~]$ sudo fail2ban-client unban 10.102.1.12
1
[marty@web ~]$ sudo fail2ban-client status sshd
Status for the jail: sshd
[...]
`- Actions
   |- Currently banned: 0
   |- Total banned:     1
   `- Banned IP list:
```
