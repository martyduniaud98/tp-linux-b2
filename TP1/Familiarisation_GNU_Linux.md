# TP1 : (re)Familiaration avec un système GNU/Linux


## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
  - carte réseau dédiée
  ```
  [marty@node1 ~]$ ip a | grep "enp0s8"
    3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.0.3.15/24 brd 10.0.3.255 scope global dynamic noprefixroute enp0s8
  ```

  - route par défaut
  ```
  [marty@node1 ~]$ ip route s | grep 10.101
    10.101.1.0/24 dev enp0s3 proto kernel scope link src 10.101.1.11 metric 100
  ```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)
  - carte réseau dédiée (host-only sur VirtualBox)
  - les machines doivent posséder une IP statique sur l'interface host-only
  ```
  [marty@node1 ~]$ ip a | grep enp0s3
    2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s3
  ```

- **vous n'utilisez QUE `ssh` pour administrer les machines**

- **les machines doivent avoir un nom**
  - référez-vous au mémo
  - les noms que doivent posséder vos machines sont précisés dans le tableau plus bas
  ```
  [marty@node1 ~]$ sudo nano /etc/hostname 
  [marty@node1 ~]$ cat /etc/hostname 
    node1.tp1.b2
  ```

- **utiliser `1.1.1.1` comme serveur DNS**
  - référez-vous au mémo
  ```
  [marty@node1 ~]$ sudo nano /etc/resolv.conf 
  [marty@node1 ~]$ cat /etc/resolv.conf | grep nameserver
    nameserver 1.1.1.1
  ```
  
  - vérifier avec le bon fonctionnement avec la commande `dig`
    - avec `dig`, demander une résolution du nom `ynov.com`
    - mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé
    ```
    [marty@node1 ~]$ dig ynov.com | grep "ynov.com"
      [...]
      ynov.com.               285     IN      A       172.67.74.226
      ynov.com.               285     IN      A       104.26.11.233
      ynov.com.               285     IN      A       104.26.10.233
    ```

    - mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu
    ```
    [marty@node1 ~]$ dig ynov.com | grep SERVER:
      ;; SERVER: 1.1.1.1#53(1.1.1.1)
    ```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**
  - fichier `/etc/hosts`
  ```
  [marty@node1 ~]$ sudo nano /etc/hosts 
    10.101.1.12 node2.tp1.b2
  ```

  - assurez-vous du bon fonctionnement avec des `ping <NOM>`
  ```
  [marty@node1 ~]$ ping node2.tp1.b2
    PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
    64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=1.22 ms
  ```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
  - commande `firewall-cmd`
  ```
  [marty@node1 ~]$ systemctl status firewalld | grep Active:
    Active: active (running) since Mon 2022-11-14 16:34:28 CET; 16min ago
  ```

Pour le réseau des différentes machines (ce sont les IP qui doivent figurer sur les interfaces host-only):

| Name               | IP            |
|--------------------|---------------|
| 🖥️ `node1.tp1.b2` | `10.101.1.11` |
| 🖥️ `node2.tp1.b2` | `10.101.1.12` |
| Votre hôte         | `10.101.1.1`  |

## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memos/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

- précisez des options sur la commande d'ajout pour que :
  - le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
  - le shell de l'utilisateur soit `/bin/bash`
  ```
  [marty@node1 ~]$ sudo useradd test -m -s /bin/bash -u 2000
    useradd test -m -s /bin/bash -u 2000
  ```
- prouvez que vous avez correctement créé cet utilisateur
  - et aussi qu'il a le bon shell et le bon homedir
  ```
  [marty@node1 ~]$ cat /etc/passwd | grep test
    test:x:2000:2000::/home/test:/bin/bash
  ```

🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.
```
[marty@node1 ~]$ sudo usermod -aG admins test
  sudo usermod -aG admins test
```

Pour permettre à ce groupe d'accéder aux droits `root` :
- il faut modifier le fichier `/etc/sudoers`
- on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
- la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
- ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)
```
[marty@node1 ~]$ sudo visudo /etc/sudoers
[marty@node1 ~]$ sudo cat /etc/sudoers | grep "%admins"
  %admins  ALL=(ALL)     ALL
```

🌞 **Ajouter votre utilisateur à ce groupe `admins`**

> Essayez d'effectuer une commande avec `sudo` peu importe laquelle, juste pour tester que vous avez le droit d'exécuter des commandes sous l'identité de `root`. Vous pouvez aussi utiliser `sudo -l` pour voir les droits `sudo` auquel votre utilisateur courant a accès.
```
[marty@node1 ~]$ sudo ls /etc
  adjtime                  inputrc                   profile.d
  aliases                  iproute2                  protocols
  [...]
```

1. Utilisateur créé et configuré
2. Groupe `admins` créé
3. Groupe `admins` ajouté au fichier `/etc/sudoers`
4. Ajout de l'utilisateur au groupe `admins`

### 2. SSH

[Une section dédiée aux clés SSH existe dans le cours.](../../cours/SSH/README.md)

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.

🌞 **Pour cela...**

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )
  - génération de clé depuis VOTRE poste donc
  ```
  [marty@node1 ~]$ ssh-keygen -t rsa
  ```
- déposer la clé dans le fichier `/home/<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer
  - vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP
  - on peut le faire à la main
  - ou avec la commande `ssh-copy-id`
  ```
  marty@martywork-ROGFlowX13:~/.ssh$ ssh-copy-id test@10.101.1.11
    [...]
    Number of key(s) added: 1
    [...]
  ```

🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.
  ```
  marty@martywork-ROGFlowX13:~$ ssh test@10.101.1.11
    Last login: Mon Nov 14 17:29:17 2022 from 10.101.1.1
  [test@node1 ~]$ 
```

## II. Partitionnement

[Il existe une section dédiée au partitionnement dans le cours](../../cours/part/)

### 1. Préparation de la VM

⚠️ **Uniquement sur `node1.tp1.b2`.**

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.
```
[marty@node1 ~]$ lsblk
  NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
  [...]
  sdb           8:16   0    3G  0 disk 
  sdc           8:32   0    3G  0 disk 
  [...]
```

### 2. Partitionnement

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Utilisez LVM** pour...

- agréger les deux disques en un seul *volume group*
```
[marty@node1 ~]$ sudo pvcreate /dev/sdb
[marty@node1 ~]$ sudo pvcreate /dev/sdc

[marty@node1 ~]$ sudo vgcreate myData /dev/sdb
[marty@node1 ~]$ sudo vgextend myData /dev/sdc
  Volume group "myData" successfully extended
[marty@node1 ~]$ sudo vgs
  [...]
  VG     #PV #LV #SN Attr   VSize VFree
  myData   2   0   0 wz--n- 5.99g 5.99g
```
- créer 3 *logical volumes* de 1 Go chacun
```
[marty@node1 ~]$ sudo lvcreate -L 1G myData -n my_first_data                                                  
[marty@node1 ~]$ sudo lvcreate -L 1G myData -n my_second_data
[marty@node1 ~]$ sudo lvcreate -L 1G myData -n my_third_data
[marty@node1 ~]$ sudo lvs
  [...]
  my_first_data  myData -wi-a----- 1.00g                                                    
  my_second_data myData -wi-a----- 1.00g                                                    
  my_third_data  myData -wi-a----- 1.00g    
```

- formater ces partitions en `ext4`
```
[marty@node1 ~]$ sudo mkfs -t ext4 /dev/myData/my_first_data 
[marty@node1 ~]$ sudo mkfs -t ext4 /dev/myData/my_second_data 
[marty@node1 ~]$ sudo mkfs -t ext4 /dev/myData/my_third_data 
```
- monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.
```
[marty@node1 ~]$ sudo mkdir /mnt/part1
[marty@node1 ~]$ sudo mkdir /mnt/part2
[marty@node1 ~]$ sudo mkdir /mnt/part3

[marty@node1 ~]$ df -h | grep /dev/mapper/myData
  /dev/mapper/myData-my_first_data   974M   24K  907M   1% /mnt/part1
  /dev/mapper/myData-my_second_data  974M   24K  907M   1% /mnt/part2
  /dev/mapper/myData-my_third_data   974M   24K  907M   1% /mnt/part3

```

🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.
  ```
  [marty@node1 ~]$ nano /etc/fstab
    sudo nano /etc/fstab
  [marty@node1 ~]$ sudo cat /etc/fstab
    [...]
    /dev/myData/my_first_data /mnt/part1 ext4 defaults 0 0
    /dev/myData/my_second_data /mnt/part2 ext4 defaults 0 0
    /dev/myData/my_third_data /mnt/part3 ext4 defaults 0 0

  [marty@node1 ~]$ sudo umount /mnt/part1
  [marty@node1 ~]$ sudo mount -av | grep successfully
    /mnt/part1               : successfully mounted
  ```

✨**Bonus** : amusez vous avez les options de montage. Quelques options intéressantes :

- `noexec`
- `ro`
- `user`
- `nosuid`
- `nodev`
- `protect`

## III. Gestion de services

Au sein des systèmes GNU/Linux les plus utilisés, c'est *systemd* qui est utilisé comme gestionnaire de services (entre autres).

Pour manipuler les services entretenus par *systemd*, on utilise la commande `systemctl`.

On peut lister les unités `systemd` actives de la machine `systemctl list-units -t service`.

**Référez-vous au mémo pour voir les autres commandes `systemctl` usuelles.**

## 1. Interaction avec un service existant

⚠️ **Uniquement sur `node1.tp1.b2`.**

Parmi les services système déjà installés sur Rocky, il existe `firewalld`. Cet utilitaire est l'outil de firewalling de Rocky.

🌞 **Assurez-vous que...**

- l'unité est démarrée
- l'unitée est activée (elle se lance automatiquement au démarrage)

## 2. Création de service

![Création de service systemd](./pics/create_service.png)

### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 

- le fichier `web.service`
- dans le répertoire `/etc/systemd/system`

Déposer le contenu suivant :

```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'ouvrir ce port dans le firewall.**

Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :

```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :

```bash
$ sudo systemctl status web
$ sudo systemctl start web
$ sudo systemctl enable web
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

- avec un navigateur depuis votre PC
- ou la commande `curl` depuis l'autre machine (je veux ça dans le compte-rendu :3)
- sur l'IP de la VM, port 8888

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

- créer un utilisateur `web`
- créer un dossier `/var/www/meow/`
- créer un fichier dans le dossier `/var/www/meow/` (peu importe son nom ou son contenu, c'est pour tester)
- montrez à l'aide d'une commande les permissions positionnées sur le dossier et son contenu

> Pour que tout fonctionne correctement, il faudra veiller à ce que le dossier et le fichier appartiennent à l'utilisateur `web` et qu'il ait des droits suffisants dessus.

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

- `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
- `WorkingDirectory=` afin de lancer le serveur depuis le dossier créé au dessus : `/var/www/meow/`
- ces deux clauses sont à positionner dans la section `[Service]` de votre unité

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**

